mini-ITX Adapter for rosco_m68k
===

This is a bracket to mount a rosco_m68k classic in a small mini-ITX case. It's been designed in FreeCAD, and made to be 3D-printed.

This is designed for the "ITX Computer Case TX02", which has a 25.4 mm tall I/O aperture, and two holes above the I/O aperture which are used to secure the bracket.

The complete board has been split into four functional pieces, because this was made to be printed on a 3D printer with a 10 cm diameter bed. Holes designed for mounting rear connectors are sized for threading with a tap. The bracket is mounted in the case with the standard ATX motherboard mounting holes.

The [64-pin](https://www.digikey.ca/en/products/detail/assmann-wsw-components/H3BWH-6406G/1043936) and [10-pin](https://www.digikey.ca/en/products/detail/assmann-wsw-components/H3WWH-1006G/1218675) IDC connectors are from Assmann WSW Components, purchased pre-assembled on a 6" ribbon cable from Digikey. They're mounted with M2.5 bolts threaded into the plastic. The [long eject levers](https://www.digikey.ca/en/products/detail/assmann-wsw-components/AWHCLIP-LONG/931202) are used. The 10-pin connector is used to pass in front-panel audio.

The rear USB connector, used for an UART over USB, is [from Adafruit](https://www.adafruit.com/product/3318), and is mounted with the included M3 bolts, threaded into the plastic.

The microSD card board is mounted with what might be M1.5 fasteners. They are bolted through the bracket.

A dual USB to UART board based on the CH559 is connected to UART B and to the two front USB connectors.

Front panel connections were pade with test clips to the RUN LED, LED1, and to the reset button. External current-limiting resistors were used. Soldering wires to the underside of the mainboard would be simpler and more robust.

Pictures
---

Top (without cover):

![A mini-ITX case, viewed from the top, with a rosco_m68k covered by several bundles of wires.](images/top.jpg "Top (without cover)")

Rear:

![A mini-ITX case, viewed from the rear. At the top left, there is a 10-pin grey panel-mount IDC connector. At the bottom left there is a panel-mount Mini-B USB connector. At the top right there is a 64-pin grey panel-mount IDC connector with eject latches. At the centre bottom, there is a microSD card board with a microSD card installed.](images/rear.jpg "Rear")
